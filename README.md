# Connect 4
[![DeepSource](https://deepsource.io/gl/ritwikgopi/connect-4.svg/?label=active+issues&show_trend=true)](https://deepsource.io/gl/ritwikgopi/connect-4/?ref=repository-badge)

This project will develop a connect 4 game with intelligent bot.


## INSTALL

To install connect 4 on your machine use the command below

Required python version python 3.6 or above

```
pip install connect-4
```

To play the game run the command below after installation

```
connect_4
```